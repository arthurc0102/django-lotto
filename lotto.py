import random


def user_choice():
    user_inputs = []
    while len(user_inputs) < 6:
        i = input('Input {}: '.format(len(user_inputs) + 1))
        if not i.isdigit():
            print('No number.')
            continue

        i = int(i)

        if not (1 <= i <= 49):
            print('Number should between 1 and 49.')
            continue

        if i in user_inputs:
            print('number already exist')
            continue

        user_inputs.append(i)

    return user_choice


def auto_choice():
    return random.sample(range(1, 50), 6)


ans = auto_choice()

print('Auto choice or yourself?')
choice_type = input('"a" for auto else for choice by yourself: ')

user_inputs = auto_choice() if choice_type == 'a' else user_choice()

total = 0
for i in user_inputs:
    if i in ans:
        total += 1

print('You got {} numbers!'.format(total))
