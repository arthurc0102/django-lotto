import random


def user_input():
    while True:
        guess = input('Choice a number, number length must be 4 and unique: ')
        if len(guess) != 4:
            print('Input length must be 4.')
            continue

        try:
            guess = [int(i) for i in guess]
        except ValueError:
            print('This is not a number.')
            continue

        if len(set(guess)) != 4:
            print('Number is not unique.')
            continue

        return guess


def main():
    ans = random.sample(range(10), 4)
    while True:
        a, b = 0, 0
        guess = user_input()

        for i, j in zip(ans, guess):
            if i == j:
                a += 1

            if j in ans:
                b += 1

        b -= a
        print('{}A{}B'.format(a, b))

        if a == 4:
            print('You got it!')
            break


if __name__ == '__main__':
    main()
